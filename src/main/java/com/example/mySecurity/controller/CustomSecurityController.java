package com.example.mySecurity.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CustomSecurityController {
    @ResponseBody
    @RequestMapping("/adminOnly")
    public String adminOnly(Authentication auth) {
        return "Secret Page!!"
                + "<br>my roles: "+ auth.getAuthorities().toString();
    }
    @ResponseBody
    @RequestMapping("/userOnly")
    public String userOnly(Authentication auth) {
        return "USER ONLY"
                + "<br>my roles: "+ auth.getAuthorities().toString();
    }
    @ResponseBody
    @RequestMapping("/userOnly/{sub}")
    public String userOnlySub(Authentication auth, @PathVariable("sub") String sub) {
        return "USER ONLY SUB PAGE (" + sub + ")"
                + "<br>my roles: "+ auth.getAuthorities().toString();
    }
    @ResponseBody
    @RequestMapping("/everybodyOK")
    public String everybodyOK() {
        return "EVERYBODY OK";
    }
}
