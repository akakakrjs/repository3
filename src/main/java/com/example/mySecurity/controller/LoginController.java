package com.example.mySecurity.controller;

import com.example.mySecurity.dao.SimpleUserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController {

    @Autowired
    SimpleUserDAO sud;
    @RequestMapping("/")
    public String home(Model model) {
        Object currentAuth = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails principal = null;
        try {
            if(!(currentAuth instanceof String)) {
                principal = (UserDetails) currentAuth;
                List<Map<String, ?>> userInfo = sud.getUserInfo(principal.getUsername());
                String food = (String) userInfo.get(0).get("food");
                model.addAttribute("food", food);
            }
        } catch(Exception e) {

        }


        return "home";
    }

    @ResponseBody
    @RequestMapping("/test")
    public String test() {
        return "OK";
    }

    @RequestMapping("/login")
    public String loginForm() {
        return "login-form";
    }


    /**
     * 로그인 폼을 거치지 않고 바로 로그인
     * @param username
     * @return
     */
    @RequestMapping("/loginWithoutForm/{username}")
    public String loginWithoutForm(@PathVariable(value="username") String username) {

        List<GrantedAuthority> roles = new ArrayList<>(1);
        String roleStr = username.equals("admin") ? "ROLE_ADMIN" : "ROLE_GUEST";
        roles.add(new SimpleGrantedAuthority(roleStr));

        User user = new User(username, "", roles);

        Authentication auth = new UsernamePasswordAuthenticationToken(user, null, roles);
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/";
    }
}