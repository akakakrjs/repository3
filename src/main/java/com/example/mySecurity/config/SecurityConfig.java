package com.example.mySecurity.config;

import com.example.mySecurity.dao.SecurityDAO;
import com.example.mySecurity.security.TaskImplementingLogoutHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired private SecurityDAO sd;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        setAntMatchers(http,"ROLE_");

        http.csrf().disable()
                /*
                .authorizeRequests()
                .antMatchers("/adminOnly").hasAuthority("ROLE_ADMIN")
                .antMatchers("/**").permitAll()  // 넓은 범위의 URL을 아래에 배치한다.
                .anyRequest().authenticated()
                .and()*/
                .formLogin().loginPage("/login").failureUrl("/login?error").permitAll()
                .defaultSuccessUrl("/")
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .addLogoutHandler(new TaskImplementingLogoutHandler()).permitAll().logoutSuccessUrl("/");
        // 로그아웃 기본 url은 (/logout)
        // 새로 설정하려면 .logoutUrl("url") 사용

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .rolePrefix("ROLE_")
                .usersByUsernameQuery("select username, replace(password, '$2y', '$2a'), true from users where username = ?")
                .authoritiesByUsernameQuery("select username, role from simple_users where username = ?");

    }

    // passwordEncoder() 추가
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    protected void setAntMatchers(HttpSecurity http, String rolePrefix) throws Exception {
        List<Map<String, Object>> list = sd.getAuthReq();
        System.out.println(list);
        for(Map<String, Object> m : list) {
            // 쉼표(,)로 구분된 권한 정보를 분리 후 배열로 저장
            String[] roles = m.get("hasAuthority").toString().split(",");
            // 권한 앞에 접두사(rolePrefix) 붙임
            for(int i = 0; i < roles.length; i++) {
                roles[i] = rolePrefix + roles[i].toUpperCase();
            }
            // DB에서 url을 읽어올 때 앞에 '/'이 없다면
            // 앞에 '/'를 넣어준다.
            String url = m.get("url").toString();
            if(url.charAt(0) != '/') {
                url = "/" + url;
            }
            // url, 권한 정보를 넣는다.
            http.authorizeRequests()
                    .antMatchers(url)
                    .hasAnyAuthority(roles);
        }
        http.authorizeRequests()
                .antMatchers("/**").permitAll()
                .anyRequest().authenticated();
    }
}